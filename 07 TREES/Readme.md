# 07 TREES

### 18 BINARY SEARCH TREES

- https://leetcode.com/problems/search-in-a-binary-search-tree/

- https://leetcode.com/problems/lowest-common-ancestor-of-a-binary-search-tree/



### 19 BST INSERT AND Remove

- https://leetcode.com/problems/insert-into-a-binary-search-tree/

- https://leetcode.com/problems/delete-node-in-a-bst/



### 20 Depth-First Search

- https://leetcode.com/problems/binary-tree-inorder-traversal/

- https://leetcode.com/problems/kth-smallest-element-in-a-bst/

- https://leetcode.com/problems/construct-binary-tree-from-preorder-and-inorder-traversal/



### 21 Breadth-First Search

- https://leetcode.com/problems/binary-tree-level-order-traversal/

- https://leetcode.com/problems/binary-tree-right-side-view/