# 02 ARRAYS

### 03 STATIC ARRAYS

- https://leetcode.com/problems/remove-duplicates-from-sorted-array/

- https://leetcode.com/problems/remove-element/

- https://leetcode.com/problems/shuffle-the-array/




### 04 DYNAMIC ARRAYS

- https://leetcode.com/problems/concatenation-of-array/




### 05 STACKS

- https://leetcode.com/problems/baseball-game/

- https://leetcode.com/problems/valid-parentheses/

- https://leetcode.com/problems/min-stack/