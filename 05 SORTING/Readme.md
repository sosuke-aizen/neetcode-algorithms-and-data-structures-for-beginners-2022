# 05 SORTING



### 11 INSERTION SORT

- https://leetcode.com/problems/sort-an-array/



### 12 MERGE SORT

- https://leetcode.com/problems/sort-an-array/

- https://leetcode.com/problems/merge-k-sorted-lists/



### 13 QUICK SORT

- https://leetcode.com/problems/sort-an-array/

- https://leetcode.com/problems/kth-largest-element-in-an-array/



### 14 BUCKET SORT

- https://leetcode.com/problems/sort-colors/