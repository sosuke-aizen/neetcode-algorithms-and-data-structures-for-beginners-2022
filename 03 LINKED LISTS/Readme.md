# 03 LINKED LISTS


### 06 SINGLY LINKED LISTS

- https://leetcode.com/problems/reverse-linked-list/

- https://leetcode.com/problems/merge-two-sorted-lists/



### 07 DOUBLY LINKED LISTS

- https://leetcode.com/problems/design-linked-list/

- https://leetcode.com/problems/design-browser-history/



### 08 QUEUES

- https://leetcode.com/problems/number-of-students-unable-to-eat-lunch/

- https://leetcode.com/problems/implement-stack-using-queues/