# 11 GRAPHS


### 30 Matrix DFS

- https://leetcode.com/problems/number-of-islands/

- https://leetcode.com/problems/max-area-of-island/


### 31 Matrix BFS

- https://leetcode.com/problems/shortest-path-in-binary-matrix/

- https://leetcode.com/problems/rotting-oranges/


### 32 Adjacency List

- https://leetcode.com/problems/clone-graph/

- https://leetcode.com/problems/course-schedule/