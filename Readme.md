# NeetCode -Algorithms & Data Structures for Beginners 2022

[01 ABOUT](./01-ABOUT/Readme.md)

[02 ARRAYS](./02-ARRAYS/Readme.md)

[03 LINKED LISTS](./03-LINKED-LISTS/Readme.md)

[04 RECURSION](./04-RECURSION/Readme.md)

[05 SORTING](./05-SORTING/Readme.md)

[06 BINARY SEARCH](./06-BINARY-SEARCH/Readme.md)

[07 TREES](./07-TREES/Readme.md)

[08 BACKTRACKING](./08-BACKTRACKING/Readme.md)

[09 HEAP PRIORITY QUEUE](./09-HEAP-PRIORITY-QUEUE/Readme.md)

[10 HASHING](./10-HASHING/Readme.md)

[11 GRAPHS](./11-GRAPHS/Readme.md)

[12 DYNAMIC PROGRAMMING](./12-DYNAMIC-PROGRAMMING/Readme.md)

[13 BIT MANIPULATION](./13-BIT-MANIPULATION/Readme.md)
