# 12 DYNAMIC PROGRAMMING

### 33 1-Dimension DP

- https://leetcode.com/problems/climbing-stairs/

- https://leetcode.com/problems/house-robber/



### 34 2-Dimension DP

- https://leetcode.com/problems/unique-paths/

- https://leetcode.com/problems/unique-paths-ii/

- https://leetcode.com/problems/longest-common-subsequence/